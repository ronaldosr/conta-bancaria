package br.com.itau;

import java.util.Map;

public class Main {

    public static void main(String[] args) {
	    // Imprimir a mensagem de boas vindas
        IO.imprimirMensagemInicial();

        // Solicitar dados para a criação da conta
        Map<String, String> dados = IO.solicitarDados();

        // Construção do cliente
        if (Utilitarios.validarIdade(Integer.parseInt(dados.get("idade")))){
            Cliente cliente = new Cliente(
                    dados.get("cpf"),
                    dados.get("nome"),
                    Integer.parseInt(dados.get("idade"))
            );

            // Criar uma nova conta
            Conta conta = new Conta(1);

            // Associar a conta ao cliente
            ClienteConta clienteConta = new ClienteConta(cliente, conta);   

            // Loop do menu de opções
            Menu menu = new Menu(clienteConta);


        } else {
            System.out.println("Não é permitido cliente com menos de 18 anos.");
        }


    }

}
