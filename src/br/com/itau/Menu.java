package br.com.itau;

import java.util.Map;

public class Menu {

    public Menu(ClienteConta clienteConta){

        int opcao = 99;

        while (opcao != 0){
            opcao = IO.menuOpcoes();

            if (opcao == 1) {
                // Consulta de saldo
                System.out.println("");
                System.out.println("Saldo atual....: " + clienteConta.getConta().getSaldo());
                System.out.println("");

            } else if (opcao == 2) {
                // Aplicação
                System.out.println("Efetuar depósito...");
                clienteConta.getConta().deposito(IO.obtemValor());
                System.out.println("Saldo após depósito...." + clienteConta.getConta().getSaldo());
                System.out.println("");

            } else if (opcao == 3) {
                // Saque
                System.out.println("Efetuar saque...");
                clienteConta.getConta().sacar(IO.obtemValor());
                System.out.println("Saldo após saque..." + clienteConta.getConta().getSaldo());
                System.out.println("");

            } else if (opcao == 0) {
                System.out.println("Bye....");

            } else {
                System.out.println("Opção inválida");
                System.out.println("");
            }
        }

    }
}
