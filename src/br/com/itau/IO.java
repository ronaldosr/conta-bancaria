package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO {

    public static void imprimirMensagemInicial() {
        System.out.println("Bem vindo ao sistema de conta bancária.");
    }

    public static Map<String, String> solicitarDados() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite o CPF do cliente:");
        String cpf = scanner.nextLine();

        System.out.println("");
        System.out.println("Digite o Nome do cliente: ");
        String nome = scanner.nextLine();

        System.out.println("");
        System.out.println("Digite a Idade do cliente: ");
        Integer idade = scanner.nextInt();

        Map<String, String> dados = new HashMap<>();
        dados.put("nome", nome);
        dados.put("cpf", cpf);
        dados.put("idade", idade.toString());

        return dados;
    }

    public static int menuOpcoes(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Qual operação você deseja executar?");
        System.out.println("(1) - Consultar saldo");
        System.out.println("(2) - Realizar depósito");
        System.out.println("(3) - Realizar saque");
        System.out.println("-----------------------------------");
        System.out.println("(0) - Sair");
        return scanner.nextInt();
    }

    public static double obtemValor() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }
}
