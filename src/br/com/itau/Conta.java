package br.com.itau;

public class Conta {

    private int id;
    private double saldo;

    public Conta(int id) {
        this.id = id;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public double sacar(double valor){
        if (saldo >= valor) {
            return saldo = saldo - valor;
        }
        System.out.println("Não há saldo suficiente para efetuar o saque.");
        return 0;
    }

    public double deposito(double valor) {
        if (saldo >= 0) {
            saldo = saldo + valor;
            return saldo;
        }
        System.out.println("O deṕosito deve ser positivo.");
        return saldo;
    }


}
